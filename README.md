# About

Pager Parser is a service designed to act as a "central hub" for pager messages. Pager Parser receives pager messages, parses them, stores them and can initiate actions based on their contents.

# Features
- Flexible framework designed to receive pager messages from a variety of data sources such as [PagerMon](https://github.com/pagermon/pagermon) ([Jobyyy](https://jobyyy.net/))
- Parsing of useful information from pager messages such as job type, location, etc
- Storage of received pager messages for archival purposes
- Storage of parsed pager message data for archival as well as generating useful metrics
- Integration with Home Assistent REST API for generating events on the event bus of configured Home Assistant servers
- Discord bot that allows users to subscribe and unsubscribe from pager message notifications

# Building

### Pre-requisites

To build the project, the .NET 8.0 SDK is required, as well as Docker if building the project as a Docker container

### Docker

To build the project using [Docker](https://docker.com/), simply execute the following command from the project base directory:

```sh
docker build -t pagerparser .
```

### Standalone

To build the project without Docker, simply execute the following command from the project base directory:

```sh
dotnet build -c Release
```

The resulting output files may be found in the `bin/Release` directory

# Deployment

### Configuration

To configure the project, simply edit the `appsettings.json` file, populating the desired sections. If using Docker, configuration should be done in the `compose.yaml` file. The provided file may be used as a template.

### Docker

To deploy the project using Docker, simply execute the following command from the project base directory:
```sh
docker compose up -d
```
### Standalone

Standalone deployments of the project are current not supported in any official capacity. It is possible to run the project as a standalone deployment by registering the main executable to be started automatically and persisted in the background. This may be achieved on Linux using a systemd unit file or on Windows as an auto-start program.
