﻿using Microsoft.EntityFrameworkCore;

namespace PagerParser;

public partial class PagerContext : DbContext {
    public DbSet<PagerMessage>       PagerMessages       { get; set; }
    public DbSet<ParsedPagerMessage> ParsedPagerMessages { get; set; }
    public DbSet<GpsPosition>        GpsPositions        { get; set; }

    private IConfiguration config;

    public PagerContext(IConfiguration config) =>
        this.config = config;

    protected override void OnConfiguring(DbContextOptionsBuilder options) =>
        options.UseNpgsql(config.GetConnectionString("DefaultConnection"));
}