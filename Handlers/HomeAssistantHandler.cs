﻿using System.Net.Http.Headers;

namespace PagerParser.Handlers;

[PagerHandler]
internal class HomeAssistantHandler : IPagerHandler {
    private List<HomeAssistantConfig> haConfigs = new();

    private ILogger logger;

    private HttpClient httpClient;

    public async Task HandleMessageAsync(PagerMessage message, ParsedPagerMessage? pm) {
        // Generate an event via the API on each configured Home Assistant server
        foreach(var ha in haConfigs) {
            var url = $"https://{ha.Host}/api/events/{ha.EventType}";
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", ha.ApiKey);
            await httpClient.PostAsJsonAsync(
                url,
                message);
        }
    }

    public void OnConfiguring(
        ILogger logger,
        IConfiguration config,
        IServiceProvider serviceProvider) {

        this.logger = logger;

        config.Bind("PagerParser:HomeAssistant:Servers", haConfigs);
        if(haConfigs is null)
            return;

        logger.LogInformation($"{haConfigs.Count()} Home Assistant servers configured");
    }

    public Task StartAsync(CancellationToken ct) {
        httpClient = new();
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken ct) {
        httpClient?.Dispose();
        return Task.CompletedTask;
    }

    private class HomeAssistantConfig {
        public string Host      { get; set; }
        public string ApiKey    { get; set; }
        public string EventType { get; set; } = "cfa_pager_message";
    }
}
