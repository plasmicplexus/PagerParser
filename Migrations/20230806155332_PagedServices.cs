﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PagerParser.Migrations
{
    /// <inheritdoc />
    public partial class PagedServices : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<List<string>>(
                name: "PagedServices",
                table: "ParsedPagerMessages",
                type: "text[]",
                defaultValue: new List<string>(),
                nullable: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PagedServices",
                table: "ParsedPagerMessages");
        }
    }
}
