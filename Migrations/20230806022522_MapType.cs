﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PagerParser.Migrations
{
    /// <inheritdoc />
    public partial class MapType : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MelwaysMapNo",
                table: "ParsedPagerMessages",
                newName: "MapType");

            migrationBuilder.RenameColumn(
                name: "MelwaysGrid",
                table: "ParsedPagerMessages",
                newName: "MapGrid");

            migrationBuilder.AddColumn<int>(
                name: "MapNo",
                table: "ParsedPagerMessages",
                type: "integer",
                nullable: true);

            migrationBuilder.Sql(
                "UPDATE \"ParsedPagerMessages\" SET \"MapType\" = 1");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MapNo",
                table: "ParsedPagerMessages");

            migrationBuilder.RenameColumn(
                name: "MapType",
                table: "ParsedPagerMessages",
                newName: "MelwaysMapNo");

            migrationBuilder.RenameColumn(
                name: "MapGrid",
                table: "ParsedPagerMessages",
                newName: "MelwaysGrid");
        }
    }
}
