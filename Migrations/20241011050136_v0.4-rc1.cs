﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PagerParser.Migrations
{
    /// <inheritdoc />
    public partial class v04rc1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DiscordAlertChannels",
                columns: table => new
                {
                    DiscordAlertChannelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GuildId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    ChannelId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    RequireMention = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordAlertChannels", x => x.DiscordAlertChannelId);
                });

            migrationBuilder.CreateTable(
                name: "DiscordAlertSubscriptions",
                columns: table => new
                {
                    DiscordAlertSubscriptionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PageDestination = table.Column<string>(type: "text", nullable: false),
                    GuildId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    PrincipalId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    PrincipalType = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordAlertSubscriptions", x => x.DiscordAlertSubscriptionId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiscordAlertChannels_GuildId",
                table: "DiscordAlertChannels",
                column: "GuildId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DiscordAlertSubscriptions_GuildId",
                table: "DiscordAlertSubscriptions",
                column: "GuildId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiscordAlertChannels");

            migrationBuilder.DropTable(
                name: "DiscordAlertSubscriptions");
        }
    }
}
