﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PagerParser.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GpsPositions",
                columns: table => new
                {
                    GpsPositionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Latitude = table.Column<double>(type: "double precision", nullable: false),
                    Longitude = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GpsPositions", x => x.GpsPositionId);
                });

            migrationBuilder.CreateTable(
                name: "PagerMessages",
                columns: table => new
                {
                    PagerMessageId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PagerMessages", x => x.PagerMessageId);
                });

            migrationBuilder.CreateTable(
                name: "ParsedPagerMessages",
                columns: table => new
                {
                    ParsedPagerMessageId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirecomJobNo = table.Column<int>(type: "integer", nullable: false),
                    AssignmentArea = table.Column<string>(type: "text", nullable: false),
                    JobType = table.Column<string>(type: "text", nullable: false),
                    AlertLevel = table.Column<int>(type: "integer", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    MelwaysMapNo = table.Column<int>(type: "integer", nullable: true),
                    MelwaysGrid = table.Column<string>(type: "text", nullable: true),
                    GridReference = table.Column<int>(type: "integer", nullable: true),
                    AttendingServices = table.Column<int>(type: "integer", nullable: false),
                    Note = table.Column<string>(type: "text", nullable: true),
                    FireGroundChannel = table.Column<int>(type: "integer", nullable: true),
                    PageDestination = table.Column<string>(type: "text", nullable: false),
                    GpsPositionId = table.Column<int>(type: "integer", nullable: true),
                    PagerMessage = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParsedPagerMessages", x => x.ParsedPagerMessageId);
                    table.ForeignKey(
                        name: "FK_ParsedPagerMessages_GpsPositions_GpsPositionId",
                        column: x => x.GpsPositionId,
                        principalTable: "GpsPositions",
                        principalColumn: "GpsPositionId");
                    table.ForeignKey(
                        name: "FK_ParsedPagerMessages_PagerMessages_PagerMessage",
                        column: x => x.PagerMessage,
                        principalTable: "PagerMessages",
                        principalColumn: "PagerMessageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PagerMessages_Message",
                table: "PagerMessages",
                column: "Message");

            migrationBuilder.CreateIndex(
                name: "IX_ParsedPagerMessages_FirecomJobNo",
                table: "ParsedPagerMessages",
                column: "FirecomJobNo");

            migrationBuilder.CreateIndex(
                name: "IX_ParsedPagerMessages_GpsPositionId",
                table: "ParsedPagerMessages",
                column: "GpsPositionId");

            migrationBuilder.CreateIndex(
                name: "IX_ParsedPagerMessages_PagerMessage",
                table: "ParsedPagerMessages",
                column: "PagerMessage",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ParsedPagerMessages");

            migrationBuilder.DropTable(
                name: "GpsPositions");

            migrationBuilder.DropTable(
                name: "PagerMessages");
        }
    }
}
