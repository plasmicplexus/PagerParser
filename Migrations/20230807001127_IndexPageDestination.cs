﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PagerParser.Migrations
{
    /// <inheritdoc />
    public partial class IndexPageDestination : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ParsedPagerMessages_PageDestination",
                table: "ParsedPagerMessages",
                column: "PageDestination");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ParsedPagerMessages_PageDestination",
                table: "ParsedPagerMessages");
        }
    }
}
