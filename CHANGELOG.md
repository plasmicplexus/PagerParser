# Version 0.4-rc3

### Bug Fixes
Fixed bug wherein the job counter would display large negative numbers if the database is empty
### Other improvements
Added configuration examples to Docker Compose file

# Version 0.4-rc2

### Bug Fixes
- Fix bug causing subscriptions to be applied globally instead of per-guild
- Fixed typo in command descriptions for Discord Bot command parameters
### Other Improvements
- Make all Discord bot command responses ephemeral
- Fixed issue wherein template appsettings.json didn't match new syntax for 0.4-rc1

# Version 0.4-rc1

### Bug Fixes
- Modified parsing regex to remove trailing underscores from page destinations
### Known Bugs
- SocketIO client continously drops connection, triggering page scrapes
### New Features
- Added Discord functionality
- Added Docker integration
- Added ability to interface with Home Assistant to generate events for pager messages
- Added ability to parse different map types
- Added job counter webpage
- Paged services are now parsed and stored
### Other Improvements
- Added resiliency to fetch service when handling SocketIO outage events
- Don't include any debug symbols in release builds
- Updated dependent library versions

# Version 0.3

### New Features
- Added framework for realtime message handling
- Added SocketIO support to PagerMon provider for realtime message reception functionality
- Added configurable incoming message regex filters

# Version 0.2

### Bug Fixes
- Fixed bug in which pager messages fetched via PagerMon provider had timestamps that were incorrectly converted to universal time
- Paged emergency services now correctly stored as non-clashing bit-fields in the DB
- Correctly extract fireground radio channel from list of paged services
### Other Improvements
- Added missing Melways pages 109, 134 and 135
- Improved accuracy of parsing regex

# Version 0.1

Initial release
